package plus.ultra.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;

public class URLHook
{
    private static boolean skip = false;
    private static final PrintStream LOG;
    private static final DateTimeFormatter LOG_DATE_FORMATTER;
    private static final URLManager URLS;

    static
    {
        try {
            if ("1".equals(System.getProperty("ultra.core.log", "1"))) {
                new File("logs").mkdirs();
                LOG = new PrintStream(new FileOutputStream("logs/ultra-core.log", true), false);
                LOG_DATE_FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
            } else {
                LOG = null;
                LOG_DATE_FORMATTER = null;
            }
            URLS = new URLManager(System.getProperty("ultra.core.config", "ultra-core.urls"));
            String[] args = new String[10];
            System.getProperties().entrySet().forEach((property) -> {
                String key = (String)property.getKey();
                if (key.startsWith("ultra.core.arg")) {
                    key = key.substring(14);
                    int num = key.isEmpty() ? 0 : (Integer.parseInt(key)-1);
                    args[num] = (String) property.getValue();
                }
            });
            for (int i = 9; i >= 0; --i) {
                if (args[i] != null) {
                    URLS.addExtraArguments(Arrays.asList(Arrays.copyOfRange(args, 0, i+1)));
                    break;
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static URLConnection openConnection(URL url, Proxy proxy)
    {
        if (skip) {
            return null;
        }
        try {
            skip = true;
            return openActualConnection(url, proxy);
        } catch (Throwable t) {
            if (LOG == null) {
                t.printStackTrace();
            } else {
                LOG.println("   > ERROR:");
                t.printStackTrace(LOG);
                LOG.println();
            }
            return null;
        } finally {
            skip = false;
        }
    }

    private static URLConnection openActualConnection(URL url, Proxy proxy) throws IOException
    {
        if (LOG != null) {
            LOG.print(LocalDateTime.now().format(LOG_DATE_FORMATTER));
            LOG.print("; ");
            LOG.println(url);
        }
        URL newUrl = URLS.replace(url);
        if (newUrl != null) {
            if (LOG != null) {
                LOG.print("    > ");
                LOG.println(newUrl);
            }
            return newUrl.openConnection(proxy);
        }
        if (LOG != null) {
            LOG.println("    > UNKNOWN");
        }
        return url.openConnection(proxy);
    }
}
