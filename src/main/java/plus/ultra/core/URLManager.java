package plus.ultra.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Finds replaces for given URLs. Can be configured manually or by config file.
 * @see URLManager.replace()
 */
public class URLManager
{
    protected static class Match
    {
        final Pattern pattern;
        final Replacement replacement;

        Match(String pattern, Replacement replacement)
        {
            this.pattern = Pattern.compile(pattern);
            this.replacement = replacement;
        }

        Replacement getReplacement(String input)
        {
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                replacement.arguments.clear();
                for (int i = 1; i <= matcher.groupCount(); ++i) {
                    replacement.arguments.add(matcher.group(i));
                }
                return replacement;
            }
            return null;
        }
    }

    protected class Replacement
    {
        final String content;
        final boolean plain;
        final List<String> arguments = new ArrayList<>();

        Replacement(String content, boolean plain)
        {
            this.content = content;
            this.plain = plain;
        }

        String apply(String input)
        {
            if (plain) {
                return content;
            }
            List<String> formatArguments = new ArrayList<>(extraArguments);
            formatArguments.addAll(arguments);
            return String.format(content, formatArguments.toArray());
        }
    }

    protected final Map<String, Replacement> plain = new HashMap<>();
    protected final Set<Match> matches = new HashSet<>();
    protected final List<String> extraArguments = new ArrayList<>();

    /**
     * Creates URLManager and loads it's config from file with given path.
     *
     * @param configFilePath
     * @throws IOException 
     */
    public URLManager(String configFilePath) throws IOException
    {
        this(new File(configFilePath));
    }

    /**
     * Creates URLManager and loads it's config from given file.
     *
     * @param configFile
     * @throws IOException 
     */
    public URLManager(File configFile) throws IOException
    {
        load(configFile);
    }

    /**
     * Creates empty URLManager.
     */
    public URLManager()
    {
    }

    /**
     * Loads config from lines.
     *
     * @param config 
     */
    public void load(String... config)
    {
        String match = null;
        boolean plainMatch = true;
        for (String line : config) {
            boolean plainLine = true;
            if (line.charAt(1) == '|') {
                plainLine = line.charAt(0) == 'p';
                line = line.substring(2);
            }
            if (match == null) {
                match = line;
                plainMatch = plainLine;
            } else {
                Replacement replacement = new Replacement(line, plainLine);
                if (plainMatch) {
                    plain.put(match, replacement);
                } else {
                    matches.add(new Match(match, replacement));
                }
                match = null;
            }
        }
    }

    /**
     * Loads config from file content.
     *
     * @param configFile
     * @throws IOException 
     */
    public final void load(File configFile) throws IOException
    {
        if (configFile.exists()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile)))) {
                String[] config = reader.lines()
                    .map(String::trim)
                    .filter(line -> line.length() > 2 && line.charAt(0) != '#')
                    .toArray(String[]::new);
                load(config);
            }
        }
    }

    /**
     * Adds extra arguments for all non-plain replacements.
     *
     * @param arguments 
     */
    public void addExtraArguments(Collection<String> arguments)
    {
        extraArguments.addAll(arguments);
    }

    /**
     * Finds and applies replacement for url.
     *
     * @param url original url to replace
     * @return new url or null if replacement not found
     * @throws MalformedURLException 
     */
    public URL replace(URL url) throws MalformedURLException
    {
        String urlString = url.toString();
        Replacement replacement = plain.get(urlString);
        if (replacement != null) {
            return new URL(replacement.apply(urlString));
        }
        for (Match match : matches) {
            replacement = match.getReplacement(urlString);
            if (replacement != null) {
                return new URL(replacement.apply(urlString));
            }
        }
        return null;
    }
}
