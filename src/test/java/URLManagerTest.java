import java.net.URL;
import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import plus.ultra.core.URLManager;

public class URLManagerTest
{
    private static URLManager URLS;

    @BeforeClass
    public static void createManager() throws Exception
    {
        URLS = new URLManager();
        URLS.load(
            "https://auth.mojang.com/session", "https://awesome.project/auth/session",
            "r|https:\\/\\/auth\\.mojang\\.com\\/server\\??(.*)", "f|https://awesome.project/auth/server?servername=%s&%s"
        );
        URLS.addExtraArguments(Arrays.asList(new String[] { "industrial" }));
    }

    @Test
    public void testUnknown() throws Exception
    {
        assertNull("Unknown URL found", URLS.replace(new URL("https://example.com")));
    }

    @Test
    public void testPlain() throws Exception
    {
        URL url = URLS.replace(new URL("https://auth.mojang.com/session"));
        assertNotNull("Known URL not found", url);
        assertEquals("Incorrect replacement", "https://awesome.project/auth/session", url.toString());
    }

    @Test
    public void testRegex() throws Exception
    {
        URL url = URLS.replace(new URL("https://auth.mojang.com/server?username=test"));
        assertNotNull("Known URL not found", url);
        assertEquals("Incorrect replacement", "https://awesome.project/auth/server?servername=industrial&username=test", url.toString());
    }
}
